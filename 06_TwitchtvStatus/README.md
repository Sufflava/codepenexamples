## 6. Twitch.tv Status

[Twitch.tv Status](http://codepen.io/sufflavus/full/bpvbOV/) is based on 

- [Materialize 0.97.6](https://materializecss.com) 
- [jQuery 2.2.2](https://jquery.com/) 
- [Twitch.tv's JSONP API](https://github.com/justintv/Twitch-API/blob/master/v3_resources/streams.md#get-streamschannel)

![06_TwitchtvStatus](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/06_TwitchtvStatus.png)