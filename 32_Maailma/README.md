## 32. MaailmA

[MaailmA](https://codepen.io/sufflavus/full/rzRQaG/) is based on 

- [Google Maps JavaScript API](https://developers.google.com/maps/documentation/javascript/)
- [jQuery 3.2.1](https://jquery.com/)
- Icons for buttons from [icons8](https://icons8.com/)

![32_Maailma](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/32_Maailma.png)
