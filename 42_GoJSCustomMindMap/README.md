## 42. GoJS Custom MindMap. Nodes in two directions.

[GoJS Custom MindMap 2.0.4](https://codepen.io/sufflavus/full/KYzGWP) is based on 

- [GoJS Mind Map](https://gojs.net/latest/samples/mindMap.html)
- [Font Awesome v5](https://fontawesome.com/)
- [CSS](https://www.w3.org/Style/CSS/)

# Cutomized parts:
- There are two parts that render in two directions: right and left.
- Zoom on mouse wheel.
- Separate buttons for Zoom.
- Nodes' and links' style.
- Shadow around nodes.
- Expand button style.
- Tooltip with action buttons that contain svg icons.
- Event handlers: click node, expand/collapse node, add/delete btn click (see console and alerts).
- Some nodes are expanded by default.
- Blue frame is removed from main diagram and nodes.
- Filter for nodes. If filter is not empty, find nodes that match the filter and all its' direct parents and expand these nodes. Other nodes are hidden.
- The whole diagram can be moved by mouse.
- Scroll is removed from the diagram.
- It is possible to move the diagram out of bounds.
- Node links are not selectable.
- Move to the center note that has just been expanded/collapsed.

![42_GoJSCustomMindMap](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/42_GoJSCustomMindMap.png)