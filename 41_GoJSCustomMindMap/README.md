## 41. GoJS Custom MindMap

[GoJS Custom MindMap](https://codepen.io/sufflavus/full/vPxWbG) is based on 

- [GoJS Mind Map 2.0.4](https://gojs.net/latest/samples/mindMap.html)
- [Font Awesome v5](https://fontawesome.com/)
- [CSS](https://www.w3.org/Style/CSS/)

# Cutomized parts:
- Zoom on mouse wheel.
- Separate buttons for Zoom.
- Nodes' and links' style.
- Shadow around nodes.
- Expand button style.
- Tooltip with action buttons that contain svg icons.
- Event handlers: click node, expand/collapse node, add/delete btn click (see console and alerts).
- Some nodes are expanded by default.
- Blue frame is removed from main diagram and nodes.
- Filter for nodes. If filter is not empty, find nodes that match the filter and all its' direct parents and expand these nodes. Other nodes are hidden.
- Roots position is not changed while expanding/collapsing nodes.
- Nodes are ordered ascending by 'text' property.

![41_GoJSCustomMindMap](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/41_GoJSCustomMindMap.png)