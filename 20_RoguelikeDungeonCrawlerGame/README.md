## 20. Roguelike Dungeon Crawler Game

[Roguelike Dungeon Crawler Game](http://codepen.io/sufflavus/full/WxeLQe/) is based on 

- [jQuery 2.2.4](https://jquery.com/) 
- [React 15.1.0](https://reactjs.org)
- [ReactDOM 15.1.0](https://reactjs.org/docs/react-dom.html)
- [Emoji CSS](https://emoji-css.afeld.me)
- [Sass](http://sass-lang.com/)
- [Babel](https://babeljs.io/)

![20_RoguelikeDungeonCrawlerGame](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/20_RoguelikeDungeonCrawlerGame.png)
