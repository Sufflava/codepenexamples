## 39. Fieldset styling

[Fieldset styling](https://codepen.io/sufflavus/full/NzPbLX) is based on 

- [CSS](https://www.w3.org/Style/CSS/)

![39_FieldsetStyling](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/39_FieldsetStyling.png)