## 4. Local Weather

[Local Weather](http://codepen.io/sufflavus/full/QNQjPV/) is based on 

- [Bootstrap 3.3.6](https://getbootstrap.com/) 
- [jQuery 2.2.2](https://jquery.com/) 
- [ipinfo.io JSON API](http://ipinfo.io/)
- [Dark Sky API](https://darksky.net/dev/)
- [Skycons](http://darkskyapp.github.io/skycons/)

![04_LocalWeather](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/04_LocalWeather.png)