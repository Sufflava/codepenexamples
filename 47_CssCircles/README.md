## 47. CSS circles

[CSS circles](https://codepen.io/sufflavus/full/YzqjWEo/) is based on 

- [CSS](https://www.w3.org/Style/CSS/)
- [Less](http://lesscss.org/)

![47_CssCircles](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/47_CssCircles.png)