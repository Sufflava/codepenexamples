## 31. Architectural Pearls

[Architectural Pearls](https://codepen.io/sufflavus/full/QMYmjv/) is based on 

- [Google Maps JavaScript API](https://developers.google.com/maps/documentation/javascript/)
- Minimalistic modern buildings that were designed by [Alvar Aalto](https://en.wikipedia.org/wiki/Alvar_Aalto) and [Lahdelma & MahlamÃ¤ki Architects](http://www.ark-l-m.fi/)

![31_ArchitecturalPearls](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/31_ArchitecturalPearls.png)
