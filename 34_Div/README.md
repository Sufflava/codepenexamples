## 34. Div that fills a remaining horizontal space

[Div that fills a remaining horizontal space](https://codepen.io/sufflavus/full/opLJJa/) is based on 

- [CSS](https://www.w3.org/Style/CSS/)

![34_Div](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/34_Div.png)
