## 25. Scrollable table

[Scrollable table](http://codepen.io/sufflavus/full/PbwVEV/) is based on 

- [jQuery 3.1.0](https://jquery.com/) 

![25_ScrollableTable_1](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/25_ScrollableTable_1.png)
![25_ScrollableTable_2](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/25_ScrollableTable_2.png)
