## 18. Recipe Box

[Recipe Box](http://codepen.io/sufflavus/full/WwqqVr/) is based on 

- [jQuery 2.2.2](https://jquery.com/) 
- [Materialize 0.97.6](https://materializecss.com) 
- [React 15.0.2](https://reactjs.org) 
- [ReactDOM 15.0.2](https://reactjs.org/docs/react-dom.html)
- [Sass](http://sass-lang.com/)
- [Babel](https://babeljs.io/)

![18_RecipeBox_1](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/18_RecipeBox_1.png)
![18_RecipeBox_2](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/18_RecipeBox_2.png)
![18_RecipeBox_3](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/18_RecipeBox_3.png)