## 23. Horizontal Shuttle Lists (Angular)

[Horizontal Shuttle Lists (Angular)](http://codepen.io/sufflavus/full/WxoZzM/) is based on 

- [Angular 1.5.5](https://angular.io) 
- [Angular's jqLite](https://docs.angularjs.org/api/ng/function/angular.element)

![23_HorizontalShuttleLists](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/22_HorizontalShuttleLists.png)