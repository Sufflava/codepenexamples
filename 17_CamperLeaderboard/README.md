## 17. Camper Leaderboard

[Camper Leaderboard](http://codepen.io/sufflavus/full/RazqBE/) is based on 

- [jQuery 2.2.2](https://jquery.com/) 
- [Materialize 0.97.6](https://materializecss.com) 
- [React 15.0.2](https://reactjs.org) 
- [ReactDOM 15.0.2](https://reactjs.org/docs/react-dom.html)
- [Sass](http://sass-lang.com/)
- [Babel](https://babeljs.io/)

![17_CamperLeaderboard](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/17_CamperLeaderboard.png)
