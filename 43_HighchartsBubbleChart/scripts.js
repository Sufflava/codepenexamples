(function() {
  var lifeAreas = ["Career", "Spirituality", "Finance", "Fun", "Health", "Relationships"];
  var rates = ["0-5", "6-10", "11-15"];

  var dataUrl = "//raw.githubusercontent.com/Sufflavus/CodepenExamples/master/30_RadialChart/data.json";

  $.getJSON(dataUrl).then(drawChart);
  
  function onTickPositioner() {
    // prevent bubbles from being partly outside of the chart's area
    var sender = this;
    
    if(!sender.tickPositions.length) {
      return [-1, 0, 1];
    }    
    
    var firstTick = sender.tickPositions[0];
    var lastTick = sender.tickPositions[sender.tickPositions.length - 1];    
    var positions = sender.tickPositions.slice();
    
    if (sender.dataMin - sender.tickInterval < firstTick) {
      positions.unshift(firstTick - sender.tickInterval);
    }

    if (sender.dataMax + sender.tickInterval > lastTick) {
      positions.push(lastTick + sender.tickInterval);
    }
    
    return positions;
  }
  
  function drawChart(data) {
    console.log(data)
    Highcharts.theme = {
      colors: ['#95C471', '#35729E', '#251735', '#F3E796'],
      colorAxis: {
        maxColor: '#05426E',
        minColor: '#F3E796'
      },
      plotOptions: {
        map: {
          nullColor: '#fcfefe'
        }
      },
      navigator: {
        legend: { enabled: false },
        navigator: { enabled: false }
      }
    };

    Highcharts.setOptions(Highcharts.theme);

    Highcharts.chart('container', {
      chart: {
        type: 'bubble',        
        backgroundColor: 'transparent',
      },
      title: {
        text: null
      },
      subTitle: {
        text: null
      },      
      legend: {
        enabled: false
      },
      xAxis: {
        title: {
          style: {
            fontSize: "12px"
          },
          text: "xAxis"
        },
        startOnTick: true,
        endOnTick: true,
        showFirstLabel: false,
        showLastLabel: false,
        tickPositioner: onTickPositioner
      },
      yAxis: {
        title: {
          style: {
            fontSize: "12px"
          },
          text: "yAxis"
        },
        startOnTick: true,
        endOnTick: true,
        showFirstLabel: false,
        showLastLabel: false,
        tickPositioner: onTickPositioner
      },
      plotOptions: {
        series: {
          dataLabels: {
            enabled: true,
            borderWidth: 0,
            format: '{point.name}',
            style: {
              fontSize: "12px",
              color: "#373737",
              fontWeight: "600",
              textOutline: 'none'
            }
          }
        }
      },
      series: data.data.map(function(person) {
          return {
            name: person.name,
            data: person.points.map(function(point) {
              return {
                name: point.area,
                x: lifeAreas.indexOf(point.area),
                y: (+point.value)/5,
                z: (+point.value)/5
              };
            })
          };
        })
      });
  }
})();