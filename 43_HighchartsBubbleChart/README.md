## 43. Highcharts bubble chart.

[Highcharts bubble chart](https://codepen.io/sufflavus/full/vwbVBp) is based on 

- [Highcharts JS v8.2.0 (2020-08-20)](https://www.highcharts.com/demo/bubble)
- [jQuery 3.4.1](https://jquery.com/)

![43_HighchartsBubbleChart_1](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/43_HighchartsBubbleChart_1.png)
![43_HighchartsBubbleChart_2](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/43_HighchartsBubbleChart_2.png)