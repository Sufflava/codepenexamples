(function() {
  M.Collapsible.init($('.collapsible'));
  
  var $rgbToHexR = $("#rgbToHexR");
  var $rgbToHexG = $("#rgbToHexG");
  var $rgbToHexB = $("#rgbToHexB");
  var $rgbToHexResultText = $("#rgbToHexResultText");
  var $rgbToHexResultColor = $("#rgbToHexResultColor");
  var $btnConvertRgbToHex = $("#btnConvertRgbToHex");
  var $btnClearRgbToHex = $("#btnClearRgbToHex");
  var $btnCopyRgbToHexResult = $("#btnCopyRgbToHexResult");
    
  var $hexToRgbHex = $("#hexToRgbHex");
  var $hexToRgbResultText = $("#hexToRgbResultText");
  var $hexToRgbResultColor = $("#hexToRgbResultColor");
  var $btnConvertHexToRgb = $("#btnConvertHexToRgb");
  var $btnClearHexToRgb = $("#btnClearHexToRgb");
  var $btnCopyHexToRgbResult = $("#btnCopyHexToRgbResult");
  
  var rgbDefaultValue = "#000000";
  var hexDefaultValue = "(0, 0, 0)";
  
  $btnConvertRgbToHex.click(() => {
    var r = getPartialRgbValue($rgbToHexR);
    var g = getPartialRgbValue($rgbToHexG);
    var b = getPartialRgbValue($rgbToHexB);      
    var rgb = new RgbColour(r, g, b);
    var hex = rgb.convertToHex();
    $rgbToHexResultText.text(hex);
    $rgbToHexResultColor.css('background-color', hex);
    M.updateTextFields();
  });
  
  $btnClearRgbToHex.click(() => {
    $rgbToHexR.val("");
    $rgbToHexG.val("");
    $rgbToHexB.val("");
    $rgbToHexResultText.text(rgbDefaultValue);
    $rgbToHexResultColor.css('background-color', rgbDefaultValue);
    M.updateTextFields();
  });
  
  $btnCopyRgbToHexResult.click(() => {
    copyToClipboard($rgbToHexResultText.text());
  });
  
  $btnConvertHexToRgb.click(() => {
    var value = getHexValue($hexToRgbHex);   
    var hex = new HexColour(value);
    var rgb = hex.convertToRgb();
    $hexToRgbResultText.text(rgb);
    $hexToRgbResultColor.css('background-color', value);
    M.updateTextFields();
  });
  
  $btnClearHexToRgb.click(() => {
    $hexToRgbHex.val("");
    $hexToRgbResultText.text(hexDefaultValue);
    $hexToRgbResultColor.css('background-color', rgbDefaultValue);
    M.updateTextFields();
  });
  
  $btnCopyHexToRgbResult.click(() => {
    copyToClipboard($hexToRgbResultText.text());
  });
  
  function getPartialRgbValue($input) {
    var value = +$input.val() || 0;
    
    value = value < 0 ? 
      0 : value > 255 ? 
      255 : value;
    
    $input.val(value);
    
    return value;
  }
  
  function copyToClipboard(text) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();
    M.toast({html: `Value ${text} has been copied to clipboard`})
  }
  
  function getHexValue($input) {
    var value = $input.val();    
    value = value.startsWith("#") ? value : `#${value}`;    
    var hex = new HexColour(value);
    value = hex.isValid() ? value : "#000000";
    $input.val(value);    
    return value;
  }
  
  class HexColour {
    #hexValue = '#';
    
    constructor(hexValue) {
      this.#hexValue = hexValue;
    }
      
    isValid() {
      return /^#([0-9A-F]{3}){1,2}$/i.test(this.#hexValue);
    }
  
    convertToRgb() {
      if(!this.isValid()) {
        return null;
      }
      
      const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
      const hex = this.#hexValue.replace(shorthandRegex, (m, r, g, b) => r + r + g + g + b + b);
      const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
      
      if(!result || result.length !== 4) {
        return null;
      }
      
      const r = parseInt(result[1], 16);
      const g = parseInt(result[2], 16);
      const b = parseInt(result[3], 16);
            
      return `(${r},${g},${b})`;
    }
  }
  
  class RgbColour {
    #rValue = null;
    #gValue = null;
    #bValue = null;
    
    constructor(rValue, gValue, bValue) {
      this.#rValue = rValue;
      this.#gValue = gValue;
      this.#bValue = bValue;
    }
    
    get hexValue() {
      return this.convertToHex();
    }
    
    isPartValid(value) {
      return value >= 0 && value <= 255;
    }
    
    isValid() {      
      return this.isPartValid(this.#rValue) && this.isPartValid(this.#gValue) && this.isPartValid(this.#bValue);
    }
    
    partToHex(value) {
      const hex = value.toString(16);
      return hex.length == 1 ? "0" + hex : hex;
    }
  
    convertToHex() {
      if(!this.isValid()) {
        return null;
      }
      
      const hexValue = "#" + this.partToHex(this.#rValue) + this.partToHex(this.#gValue) + this.partToHex(this.#bValue);
      
      return hexValue;
    }
  }
})();