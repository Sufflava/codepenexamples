## 45. Color converter

[Color converter](https://codepen.io/sufflavus/full/QWyNmVZ) is based on 

- [jQuery 3.5.1](https://jquery.com/) 
- [Materialize 1.0.0](https://materializecss.com) 

![45_ColorConverter_1](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/45_ColorConverter_1.png)
![45_ColorConverter_2](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/45_ColorConverter_2.png)
![45_ColorConverter_3](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/45_ColorConverter_3.png)