## 9. Tic Tac Toe Game

[Tic Tac Toe Game](http://codepen.io/sufflavus/full/RaBqje/) is based on 

- [Materialize 0.97.6](https://materializecss.com) 
- [jQuery 2.2.2](https://jquery.com/) 
- [Case Study on Tic-Tac-Toe Part 2: With AI](http://www3.ntu.edu.sg/home/ehchua/programming/java/javagame_tictactoe_ai.html) 

![09_TicTacToeGame](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/09_TicTacToeGame.png)