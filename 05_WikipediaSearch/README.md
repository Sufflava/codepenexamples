## 5. Wikipedia Search 

[Wikipedia Search](http://codepen.io/sufflavus/full/XdZZYV/) is based on 

- [Materialize 0.97.6](https://materializecss.com) 
- [jQuery 2.2.2](https://jquery.com/) 
- [Wikipedia's API](http://www.mediawiki.org/wiki/API:Main_page)

![05_WikipediaSearch](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/05_WikipediaSearch.png)