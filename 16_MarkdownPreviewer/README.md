## 16. Markdown Previewer

[Markdown Previewer](http://codepen.io/sufflavus/full/LNKmMP/) is based on 

- [jQuery 2.2.2](https://jquery.com/) 
- [Bootstrap 3.3.6](https://getbootstrap.com/) 
- [React 15.0.2](https://reactjs.org) 
- [ReactDOM 15.0.2](https://reactjs.org/docs/react-dom.html)
- [marked 0.3.5](https://github.com/markedjs/marked)
- [Sass](http://sass-lang.com/)
- [Babel](https://babeljs.io/)

![16_MarkdownPreviewer](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/16_MarkdownPreviewer.png)
