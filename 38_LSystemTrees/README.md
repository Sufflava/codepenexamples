## 38. LSystem Trees

[L-System Trees](https://codepen.io/sufflavus/full/jzOOyr/) is based on 

- [L-system](https://en.wikipedia.org/wiki/L-system)
- [The Algorithmic Beauty of Plants](http://algorithmicbotany.org/papers/abop/abop.pdf) by Przemyslaw Prusinkiewicz and Aristid Lindenmaye
- [d3.js 4.13.0](https://d3js.org)

![38_LSystemTrees_1](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/38_LSystemTrees_1.png)
![38_LSystemTrees_2](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/38_LSystemTrees_2.png)
![38_LSystemTrees_3](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/38_LSystemTrees_3.png)
![38_LSystemTrees_4](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/38_LSystemTrees_4.png)
