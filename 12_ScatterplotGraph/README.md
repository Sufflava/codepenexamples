
## 12. Scatterplot Graph

[Scatterplot Graph](http://codepen.io/sufflavus/full/RamLEJ/) is based on 

- [d3.js v3](https://d3js.org)

![12_ScatterplotGraph](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/12_ScatterplotGraph.png)