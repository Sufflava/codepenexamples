## 50. Heat map of Finland

[Heat map of Finland](https://codepen.io/sufflavus/full/gOMwxgw) is based on 

- [GeoJson of Finland from TeemuKoivisto's repo](https://github.com/TeemuKoivisto/map-of-finland/blob/master/src/kuntarajat-2018.json)
- [Statistics of regional income tax rates in 2014 is from tomimick's repo](https://github.com/tomimick/mapcolorizer/blob/master/data-finland/data/2014-tulovero.json)
- [d3.js 6.2.0](https://d3js.org)
- [d3-geo 1.9.1](https://github.com/d3/d3-geo)
- [d3-selection 1.2.0](https://github.com/d3/d3-selection)
- [d3-array 1.2.2](https://github.com/d3/d3-array)

![50_HeatMapOfFinland_1](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/50_HeatMapOfFinland_1.png)
![50_HeatMapOfFinland_2](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/50_HeatMapOfFinland_2.png)