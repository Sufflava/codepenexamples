(function(){
  // GeoJson of Finland is from TeemuKoivisto's repo (https://github.com/TeemuKoivisto/map-of-finland/blob/master/src/kuntarajat-2018.json)
  // Statistics of regional income tax rates is from tomimick's repo (https://github.com/tomimick/mapcolorizer/blob/master/data-finland/data/2014-tulovero.json)
  
  var mapUrl = "//bitbucket.org/Sufflava/codepenexamples/raw/2534fc782ca5e8ee60959a9d303f66451c4f729a/50_HeatMapOfFinland/data/kuntarajat-2018.json";
 
  var dataUrl = "//bitbucket.org/Sufflava/codepenexamples/raw/2534fc782ca5e8ee60959a9d303f66451c4f729a/50_HeatMapOfFinland/data/2014-tulovero.json";

  var container = d3.select("#container")
    .node()
    .getBoundingClientRect(); 
  
  var width = container.width;
  var height = container.height;
  
  // https://bost.ocks.org/mike/map/ - instruction for drawing a map using d3 and geojson
  // https://stackoverflow.com/questions/45880033/mercator-causing-blank-d3-page
  var projection = d3.geoMercator()
    .scale(2000) // make the map X times bigger (the right scale should be found by hands)
    .translate([width*(-0.5) - 100, height*3.5 + 70]); // position the map inside the container (the right position should be found by hands)

  var path = d3.geoPath(projection);

  var svg = d3.select("#container")
    .append("svg")
      .attr("width", width)
      .attr("height", height)
    .append("g");
  
  // title
  svg.append("text")    
    .classed("title", true)
    .attr("x", width/2)
    .attr("y", 40) 
    .text("Regional income tax rates in 2014 (%)"); 
  
  var tooltip = d3.select("body")
    .append("div")
      .classed("tooltip", true)
      .text("");
 
  // https://stackoverflow.com/questions/49534470/d3-js-v5-promise-all-replaced-d3-queue
  var promises = [d3.json(mapUrl), d3.json(dataUrl)];

  Promise.all(promises).then(function(values) {
    var geoJson = values[0];
    var data = values[1];
    drawMap(geoJson, data);
  });
  
  function drawMap(geoJson, statData) {
    var minStatValue = 16; // Math.min(...Object.values(statData));
    var maxStatValue = 23; // Math.max(...Object.values(statData));
        
    var i = d3.scaleLinear()
      .domain([minStatValue, maxStatValue]);   
    
    var getDataCode = function(numCode) {
      var code = "";
      
      if(numCode < 10) {
        code = "00" + numCode;
      } else if (numCode < 100) {
        code = "0" + numCode;
      } else {
        code = "" + numCode;
      }
      
      return "area" + code;
    };
    
    // https://github.com/d3/d3-scale-chromatic
    var getColor = function(value) {
      return d3.interpolateBuPu(i(value));
    };
    
    svg.selectAll("path")
      .data(geoJson.features)
      .enter()
      .append("path")
        .attr("d", path)
        .classed("region", true)
        .style("fill", function (d) { 
          var value = statData[getDataCode(+d.properties.NATCODE)];
          return getColor(value); 
        })
        .style("stroke", getColor((minStatValue + maxStatValue)/2))
      .on("mouseover", function(data) {
        return tooltip
          .style("visibility", "visible")
          .text(data.properties.NAMEFIN + ": " + statData[getDataCode(+data.properties.NATCODE)] + "%");
      })
      .on("mousemove", function() {
        return tooltip
          .style("top", (event.pageY - 45) + "px")
          .style("left", event.pageX + "px")
          .style("transform", "translateX(-50%)");
      })
      .on("mouseout", function() {
        return tooltip.style("visibility", "hidden");
      });
    
    var legendTicks = d3.ticks(minStatValue, maxStatValue, 5);
    var legendRectWidth = 20;
    var legendRectHeight = 10;
    var legendX = width - 30 - legendTicks.length*legendRectWidth;
    var legendY = height - 40 - legendRectHeight;
    
    var legendWrapper = svg.append("g")
      .classed("legend-wrapper", true)
      .style("transform", "translate(" + legendX + "px, " + legendY + "px)");
    
    var extraTickForLabel = legendTicks[legendTicks.length - 1] + 1;
    legendTicks.push(extraTickForLabel);
           
    var legend = legendWrapper.selectAll(".legend-tile")
      .data(legendTicks)
      .enter()
      .append("g")
        .classed("legend-tile", true)
        .style("transform", function(d, i) { return "translate(" + (legendRectWidth) * i + "px, 0)"; });
    
    legend.append("rect")
      .attr("width", legendRectWidth)
      .attr("height", legendRectHeight) 
      .style("fill", function(d, i) { 
        return i === legendTicks.length - 1 ? "transparent" : getColor(d); 
      });
    
    legend.append("text")
      .attr("x", -5)
      .attr("y", 25) 
      .text(function(d) { return d; }); 
  }
})();