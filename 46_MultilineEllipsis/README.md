## 46. Multiline ellipsis

[Multiline ellipsis](https://codepen.io/sufflavus/full/yLezwzQ/) is based on 

- [CSS](https://www.w3.org/Style/CSS/)
- [Less](http://lesscss.org/)

![46_MultilineEllipsis](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/46_MultilineEllipsis.png)