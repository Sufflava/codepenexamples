## 3. Random Quote Machine

[Random Quote Machine](http://codepen.io/sufflavus/full/aNErgQ) is based on 

- [Bootstrap 3.3.6](https://getbootstrap.com/) 
- [Font Awesome 4.5.0](https://fontawesome.com/) 
- [jQuery 2.2.2](https://jquery.com/) 
- [Quotes on design JSON REST API](http://quotesondesign.com/api-v4-0/)

![03_RandomQuoteMachine](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/03_RandomQuoteMachine.png)
