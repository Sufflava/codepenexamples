## 7. Calculator

[Calculator](http://codepen.io/sufflavus/full/EKEOOr/) is based on 

- [Materialize 0.97.6](https://materializecss.com) 
- [jQuery 2.2.2](https://jquery.com/) 

![07_Calculator](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/07_Calculator.png)