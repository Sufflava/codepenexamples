## 2. Personal Portfolio

[Personal Portfolio](https://codepen.io/sufflavus/full/oxGexd) is based on 

- [Bootstrap 3.3.6](https://getbootstrap.com/) 
- [Font Awesome 4.5.0](https://fontawesome.com/) 
- [jQuery 2.2.2](https://jquery.com/) 
- [Bootstrap scrollspy](https://getbootstrap.com/docs/4.0/components/scrollspy/)
