## 35. Ring diagram

[Ring diagram](https://codepen.io/sufflavus/full/QQjepZ/) is based on 

- [CSS](https://www.w3.org/Style/CSS/)
- [Less](http://lesscss.org/)

![35_RingDiagram](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/35_RingDiagram.png)
