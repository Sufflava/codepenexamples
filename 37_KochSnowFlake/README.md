## 37. Koch snow flake

[Koch snow flake](https://codepen.io/sufflavus/full/wybKzP/) is based on 

- [Mathematical Koch snowflake](https://en.wikipedia.org/wiki/Koch_snowflake)
- [d3.js 4.13.0](https://d3js.org)

![37_KochSnowFlake](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/37_KochSnowFlake.png)
