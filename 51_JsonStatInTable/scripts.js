var dataSet = null;
// JSON-stat file was copyed from http://json-stat.org/samples/oecd-canada-col.json
var dataUrl = "//bitbucket.org/Sufflava/codepenexamples/raw/0774cf8b44fb7a8abab6a7e5f6659b1d3695cfac/51_JsonStatInTable/data/oecd-canada-col.json";

JSONstat(dataUrl).then(
  function(J) {  
    // There are two data sets in the source json-stat. There will be shown only one of them (the first one).
    var dataSourceIndex = 0;
    var dataSourseInfo = J.link.item[dataSourceIndex];
    var areaDimensionName = "area"; // dimension name 'area' is found in J object
    var areas = dataSourseInfo.dimension[areaDimensionName].category.label;
    
    $(".title").text(dataSourseInfo.label);
    
    //console.log(J.Dataset( 0 ));  
    //console.log(J.Dataset( 0 ).id);     
    //console.log(J.Dataset( 0 ).Dimension("concept").label); 
    //console.log(J.Dataset( 0 ).Dimension("area").label); 
    //console.log(J.Dataset( 0 ).Dimension("year").label); 
    //console.log(J.Dataset( 0 ).Dimension("area").Category(1));
    //console.log(J.Dataset(0).toTable( { type : "arrobj", content: "id", vlabel : "Unemployment rate, %" } ));    

    dataSet = J.Dataset(dataSourceIndex);
    var dataTable = dataSet.toTable({ content: "id", vlabel : "Unemployment rate, %" });
    // first row of the data is header, other rows contain actual data
    var dataHeaders = dataTable[0];
    var dataCells = dataTable.slice(1);
    
    var $table = $("<table>");
    var $header = $("<tr>").appendTo($table);
    
    dataHeaders
      .slice(1) // get all cells but the first one
      .forEach(h => {
        $("<th>").text(h).appendTo($header);
      });
    
    dataCells.forEach(cells => {
      var cellsToShow = cells.slice(1); // get all cells but the first one
      var $row = $("<tr>").data("id", cellsToShow[0]).appendTo($table); 
      
      cellsToShow.forEach((c, i) => {
        // labels for the first cell are taken form the 'areas' object
        var text = i === 0 ? areas[c]: c;
        $("<td>")
          .text(text)
          .appendTo($row);
      });
    });
    
    $("#container").append($table); 
    
    var $note = $("<div/>")
      .addClass("note")
      .text(dataSourseInfo.note);
    
    $("#container").append($note); 
    
    var $filterOptions = $(".options");
    
    var $defaultOption = $("<div/>")
        .addClass("dimension active")
        .data("id", null)
        .text("All");
      $filterOptions.append($defaultOption);

    Object.entries(areas).forEach(a => {
      const [key, value] = a;
      var $option = $("<div/>")
        .addClass("dimension")
        .data("id", key)
        .text(value);
      $filterOptions.append($option);
    });
    
    $(".dimension").click(filterTable);
  }
);

function filterTable() {
  var $this = $(this);
  $(".dimension").removeClass("active");
  $this.addClass("active");
  var areaId = $this.data("id");
  var $rows = $("table tr").not(":first"); // skip header

  if(!areaId) {
    $rows.show();
    return;
  }
  
  $rows.each(index => {
    var $row = $($rows[index]);
    var isVisible = $row.data("id") === areaId;
    
    if(isVisible) {
      $row.show();
    } else {
      $row.hide();
    }
  });
}