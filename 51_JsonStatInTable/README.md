## 51. JSON-stat in table

[JSON-stat in table](https://codepen.io/sufflavus/full/wvWgBdG) is based on 

- [JSON-stat Format](https://json-stat.org/format)
- [JSON-stat file with test data is from json-stat web site](http://json-stat.org/samples/oecd-canada-col.json)
- [jQuery 3.5.1](https://jquery.com/) 
- [jsonstat-toolkit 1.0.8](https://www.npmjs.com/package/jsonstat-toolkit)


![51_JsonStatInTable](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/51_JsonStatInTable.png)