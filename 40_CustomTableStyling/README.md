## 40. Custom table styling

[Custom table styling](https://codepen.io/sufflavus/full/zJdYdW) is based on 

- [CSS](https://www.w3.org/Style/CSS/)

![40_CustomTableStyling_1](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/40_CustomTableStyling_1.png)
![40_CustomTableStyling_2](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/40_CustomTableStyling_2.png)