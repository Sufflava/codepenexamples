# CodepenExamples

Source code of [codepen experiments](https://codepen.io/sufflavus/pens/showcase) that I have done

## 1. Tribute Page

[Tribute Page](https://codepen.io/sufflavus/full/jqGOXO) is based on 

- [Bootstrap 3.3.6](https://getbootstrap.com/)

[Source](https://bitbucket.org/Sufflava/codepenexamples/src/master/01_TributePage)

## 2. Personal Portfolio

[Personal Portfolio](https://codepen.io/sufflavus/full/oxGexd) is based on 

- [Bootstrap 3.3.6](https://getbootstrap.com/) 
- [Font Awesome 4.5.0](https://fontawesome.com/) 
- [jQuery 2.2.2](https://jquery.com/) 
- [Bootstrap scrollspy](https://getbootstrap.com/docs/4.0/components/scrollspy/)

[Source](https://bitbucket.org/Sufflava/codepenexamples/src/master/02_PersonalPortfolio)

## 3. Random Quote Machine

[Random Quote Machine](http://codepen.io/sufflavus/full/aNErgQ) is based on 

- [Bootstrap 3.3.6](https://getbootstrap.com/) 
- [Font Awesome 4.5.0](https://fontawesome.com/) 
- [jQuery 2.2.2](https://jquery.com/) 
- [Quotes on design JSON REST API](http://quotesondesign.com/api-v4-0/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/03_RandomQuoteMachine)

![03_RandomQuoteMachine](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/03_RandomQuoteMachine.png)

## 4. Local Weather

[Local Weather](http://codepen.io/sufflavus/full/QNQjPV/) is based on 

- [Bootstrap 3.3.6](https://getbootstrap.com/) 
- [jQuery 2.2.2](https://jquery.com/) 
- [ipinfo.io JSON API](http://ipinfo.io/)
- [Dark Sky API](https://darksky.net/dev/)
- [Skycons](http://darkskyapp.github.io/skycons/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/04_LocalWeather)

![04_LocalWeather](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/04_LocalWeather.png)

## 5. Wikipedia Search 

[Wikipedia Search](http://codepen.io/sufflavus/full/XdZZYV/) is based on 

- [Materialize 0.97.6](https://materializecss.com) 
- [jQuery 2.2.2](https://jquery.com/) 
- [Wikipedia's API](http://www.mediawiki.org/wiki/API:Main_page)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/05_WikipediaSearch)

![05_WikipediaSearch](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/05_WikipediaSearch.png)

## 6. Twitch.tv Status

[Twitch.tv Status](http://codepen.io/sufflavus/full/bpvbOV/) is based on 

- [Materialize 0.97.6](https://materializecss.com) 
- [jQuery 2.2.2](https://jquery.com/) 
- [Twitch.tv's JSONP API](https://github.com/justintv/Twitch-API/blob/master/v3_resources/streams.md#get-streamschannel)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/06_TwitchtvStatus)

![06_TwitchtvStatus](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/06_TwitchtvStatus.png)

## 7. Calculator

[Calculator](http://codepen.io/sufflavus/full/EKEOOr/) is based on 

- [Materialize 0.97.6](https://materializecss.com) 
- [jQuery 2.2.2](https://jquery.com/) 

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/07_Calculator)

![07_Calculator](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/07_Calculator.png)

## 8. Pomodoro Clock

[Pomodoro Clock](http://codepen.io/sufflavus/full/pyVVgV/) is based on 

- [Materialize 0.97.6](https://materializecss.com) 
- [jQuery 2.2.2](https://jquery.com/) 
- [Moment.js 2.13.0](https://momentjs.com) 

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/08_PomodoroClock)

![08_PomodoroClock](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/08_PomodoroClock.png)

## 9. Tic Tac Toe Game

[Tic Tac Toe Game](http://codepen.io/sufflavus/full/RaBqje/) is based on 

- [Materialize 0.97.6](https://materializecss.com) 
- [jQuery 2.2.2](https://jquery.com/) 
- [Case Study on Tic-Tac-Toe Part 2: With AI](http://www3.ntu.edu.sg/home/ehchua/programming/java/javagame_tictactoe_ai.html) 

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/09_TicTacToeGame)

![09_TicTacToeGame](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/09_TicTacToeGame.png)

## 10. Simon Game

[Simon Game](http://codepen.io/sufflavus/full/MyPLVB/) is based on 

- [Materialize 0.97.6](https://materializecss.com) 
- [jQuery 2.2.2](https://jquery.com/) 

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/10_SimonGame)

![10_SimonGame](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/10_SimonGame.png)

## 11. Bar Chart

[Bar Chart](http://codepen.io/sufflavus/full/EKMgBN/) is based on 

- [d3.js v3](https://d3js.org) 

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/11_BarChart)

![11_BarChart](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/11_BarChart.png)

## 12. Scatterplot Graph

[Scatterplot Graph](http://codepen.io/sufflavus/full/RamLEJ/) is based on 

- [d3.js v3](https://d3js.org)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/12_ScatterplotGraph)

![12_ScatterplotGraph](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/12_ScatterplotGraph.png)

## 13. Heat Map

[Heat Map](http://codepen.io/sufflavus/full/oxRJRb/) is based on 

- [d3.js v3](https://d3js.org)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/13_HeatMap)

![13_HeatMap](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/13_HeatMap.png)

## 14. Meteorite Landing

[Map Data Across the Globe. Meteorite landing (zoom)](http://codepen.io/sufflavus/full/pyXNwY/) is based on 

- [d3.js v3](https://d3js.org)
- [topojson v1](https://github.com/topojson/topojson)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/14_MeteoriteLanding)

![14_MeteoriteLanding](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/14_MeteoriteLanding.png)

## 15. National Contiguity
[National Contiguity](http://codepen.io/sufflavus/full/QEWqJa/) is based on 

- [d3.js 3.5.17](https://d3js.org)
- [Flag Sprites](https://www.flag-sprites.com/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/15_NationalContiguity)

![15_NationalContiguity](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/15_NationalContiguity.png)

## 16. Markdown Previewer

[Markdown Previewer](http://codepen.io/sufflavus/full/LNKmMP/) is based on 

- [jQuery 2.2.2](https://jquery.com/) 
- [Bootstrap 3.3.6](https://getbootstrap.com/) 
- [React 15.0.2](https://reactjs.org) 
- [ReactDOM 15.0.2](https://reactjs.org/docs/react-dom.html)
- [marked 0.3.5](https://github.com/markedjs/marked)
- [Sass](http://sass-lang.com/)
- [Babel](https://babeljs.io/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/16_MarkdownPreviewer)

![16_MarkdownPreviewer](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/16_MarkdownPreviewer.png)

## 17. Camper Leaderboard

[Camper Leaderboard](http://codepen.io/sufflavus/full/RazqBE/) is based on 

- [jQuery 2.2.2](https://jquery.com/) 
- [Materialize 0.97.6](https://materializecss.com) 
- [React 15.0.2](https://reactjs.org) 
- [ReactDOM 15.0.2](https://reactjs.org/docs/react-dom.html)
- [Sass](http://sass-lang.com/)
- [Babel](https://babeljs.io/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/17_CamperLeaderboard)

![17_CamperLeaderboard](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/17_CamperLeaderboard.png)

## 18. Recipe Box

[Recipe Box](http://codepen.io/sufflavus/full/WwqqVr/) is based on 

- [jQuery 2.2.2](https://jquery.com/) 
- [Materialize 0.97.6](https://materializecss.com) 
- [React 15.0.2](https://reactjs.org) 
- [ReactDOM 15.0.2](https://reactjs.org/docs/react-dom.html)
- [Sass](http://sass-lang.com/)
- [Babel](https://babeljs.io/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/18_RecipeBox)

![18_RecipeBox_3](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/18_RecipeBox_3.png)

## 19. Game of Life

[Game of Life](http://codepen.io/sufflavus/full/grVeEE/) is based on 

- [jQuery 2.2.2](https://jquery.com/) 
- [Materialize 0.97.6](https://materializecss.com) 
- [React 15.0.2](https://reactjs.org) 
- [ReactDOM 15.0.2](https://reactjs.org/docs/react-dom.html)
- [Sass](http://sass-lang.com/)
- [Babel](https://babeljs.io/)
- [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/19_GameOfLife)

![19_GameOfLife](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/19_GameOfLife.png)

## 20. Roguelike Dungeon Crawler Game

[Roguelike Dungeon Crawler Game](http://codepen.io/sufflavus/full/WxeLQe/) is based on 

- [jQuery 2.2.4](https://jquery.com/) 
- [React 15.1.0](https://reactjs.org)
- [ReactDOM 15.1.0](https://reactjs.org/docs/react-dom.html)
- [Emoji CSS](https://emoji-css.afeld.me)
- [Sass](http://sass-lang.com/)
- [Babel](https://babeljs.io/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/20_RoguelikeDungeonCrawlerGame)

![20_RoguelikeDungeonCrawlerGame](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/20_RoguelikeDungeonCrawlerGame.png)

## 21. Horizontal Splitter

[Horizontal Splitter](http://codepen.io/sufflavus/full/MeeXoR/) is based on 

- [jQuery 2.2.4](https://jquery.com/) 

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/21_HorizontalSplitter)

![21_HorizontalSplitter](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/21_HorizontalSplitter.png)

## 22. Horizontal Shuttle Lists (jQuery)

[Horizontal Shuttle Lists (jQuery)](http://codepen.io/sufflavus/full/jrrRZL/) is based on 

- [jQuery 2.2.4](https://jquery.com/) 

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/22_HorizontalShuttleLists)

![22_HorizontalShuttleLists](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/22_HorizontalShuttleLists.png)

## 23. Horizontal Shuttle Lists (Angular)

[Horizontal Shuttle Lists (Angular)](http://codepen.io/sufflavus/full/WxoZzM/) is based on 

- [Angular 1.5.5](https://angular.io) 
- [Angular's jqLite](https://docs.angularjs.org/api/ng/function/angular.element)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/23_HorizontalShuttleLists)

![23_HorizontalShuttleLists](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/23_HorizontalShuttleLists.png)

## 24. Ministry Of Truth

[Ministry Of Truth](http://codepen.io/sufflavus/full/RRkwJP/) is abandoned as it turned out that it is really hard to implement a huge project like this on CodePin

[Source](https://bitbucket.org/Sufflava/codepenexamples/src/master/24_MinistryOfTruth)

## 25. Scrollable table

[Scrollable table](http://codepen.io/sufflavus/full/PbwVEV/) is based on 

- [jQuery 3.1.0](https://jquery.com/) 

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/25_ScrollableTable)

![25_ScrollableTable_2](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/25_ScrollableTable_2.png)

## 26. Flex and dotted text 1

[Flex and dotted text 1](http://codepen.io/sufflavus/full/yVPodz) is based on 

- [CSS](https://www.w3.org/Style/CSS/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/26_FlexAndDottedText1)

![26_FlexAndDottedText1_3](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/26_FlexAndDottedText1_3.png)

## 27. Flex and dotted text 2

[Flex and dotted text 2](http://codepen.io/sufflavus/full/PbORGQ/) is based on 

- [CSS](https://www.w3.org/Style/CSS/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/27_FlexAndDottedText2)

![27_FlexAndDottedText2_1](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/27_FlexAndDottedText2_1.png)

## 28. Honeycomb

[Honeycomb](http://codepen.io/sufflavus/full/dNZOWz) is based on 

- [CSS](https://www.w3.org/Style/CSS/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/28_Honeycomb)

![28_Honeycomb](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/28_Honeycomb.png)

## 29. Honeycomb with image

[Honeycomb with image](http://codepen.io/sufflavus/full/JEpGpx/) is based on 

- [CSS](https://www.w3.org/Style/CSS/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/29_HoneycombWithImage)

![29_HoneycombWithImage](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/29_HoneycombWithImage.png)

## 30. Radial chart

[Radial chart](https://codepen.io/sufflavus/full/EvdzwO/) is based on 

- [Highcharts 5.0.14](https://www.highcharts.com/demo/column-basic) 
- [Highcharts-more 5.0.14](http://code.highcharts.com/highcharts-more.js) 
- [jQuery 3.2.1](https://jquery.com/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/30_RadialChart)

![30_RadialChart](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/30_RadialChart.png)

## 31. Architectural Pearls

[Architectural Pearls](https://codepen.io/sufflavus/full/QMYmjv/) is based on 

- [Google Maps JavaScript API](https://developers.google.com/maps/documentation/javascript/)
- Minimalistic modern buildings that were designed by [Alvar Aalto](https://en.wikipedia.org/wiki/Alvar_Aalto) and [Lahdelma & Mahlamaki Architects](http://www.ark-l-m.fi/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/31_ArchitecturalPearls)

![31_ArchitecturalPearls](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/31_ArchitecturalPearls.png)

## 32. MaailmA

[MaailmA](https://codepen.io/sufflavus/full/rzRQaG/) is based on 

- [Google Maps JavaScript API](https://developers.google.com/maps/documentation/javascript/)
- [jQuery 3.2.1](https://jquery.com/)
- Icons for buttons from [icons8](https://icons8.com/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/32_Maailma)

![32_Maailma](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/32_Maailma.png)

## 33. Material range input (SeekBar)

[Material range input](https://codepen.io/sufflavus/full/gGvpqx/) is based on 

- [jQuery 3.2.1](https://jquery.com/)
- [Material design slider spec](https://material.io/guidelines/components/sliders.html)
- [Material design color spec](https://material.io/guidelines/style/color.html)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/33_MaterialRangeInput)

![33_MaterialRangeInput](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/33_MaterialRangeInput.png)

## 34. Div that fills a remaining horizontal space

[Div that fills a remaining horizontal space](https://codepen.io/sufflavus/full/opLJJa/) is based on 

- [CSS](https://www.w3.org/Style/CSS/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/34_Div)

![34_Div](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/34_Div.png)

## 35. Ring diagram

[Ring diagram](https://codepen.io/sufflavus/full/QQjepZ/) is based on 

- [CSS](https://www.w3.org/Style/CSS/)
- [Less](http://lesscss.org/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/35_RingDiagram)

![35_RingDiagram](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/35_RingDiagram.png)

## 36. Koch Curve

[Koch Curve](https://codepen.io/sufflavus/full/GQeeOQ/) is based on 

- [Mathematical Koch Curve](https://de.wikipedia.org/wiki/Koch-Kurve)
- [d3.js 4.13.0](https://d3js.org)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/36_KochCurve)

![36_KochCurve](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/36_KochCurve.png)

## 37. Koch snow flake

[Koch snow flake](https://codepen.io/sufflavus/full/wybKzP/) is based on 

- [Mathematical Koch snowflake](https://en.wikipedia.org/wiki/Koch_snowflake)
- [d3.js 4.13.0](https://d3js.org)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/37_KochSnowFlake)

![37_KochSnowFlake](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/37_KochSnowFlake.png)

## 38. LSystem Trees

[L-System Trees](https://codepen.io/sufflavus/full/jzOOyr/) is based on 

- [L-system](https://en.wikipedia.org/wiki/L-system)
- [The Algorithmic Beauty of Plants](http://algorithmicbotany.org/papers/abop/abop.pdf) by Przemyslaw Prusinkiewicz and Aristid Lindenmaye
- [d3.js 4.13.0](https://d3js.org)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/38_LSystemTrees)

![38_LSystemTrees_1](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/38_LSystemTrees_all.png)

## 39. Fieldset styling

[Fieldset styling](https://codepen.io/sufflavus/full/NzPbLX) is based on 

- [CSS](https://www.w3.org/Style/CSS/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/39_FieldsetStyling)

![39_FieldsetStyling](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/39_FieldsetStyling.png)

## 40. Custom table styling

[Custom table styling](https://codepen.io/sufflavus/full/zJdYdW) is based on 

- [CSS](https://www.w3.org/Style/CSS/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/40_CustomTableStyling)

![40_CustomTableStyling_1](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/40_CustomTableStyling_1.png)

## 41. GoJS Custom MindMap

[GoJS Custom MindMap](https://codepen.io/sufflavus/full/vPxWbG) is based on 

- [GoJS Mind Map 2.0.4](https://gojs.net/latest/samples/mindMap.html)
- [Font Awesome v5](https://fontawesome.com/)
- [CSS](https://www.w3.org/Style/CSS/)

### Cutomized parts:
- Zoom on mouse wheel.
- Separate buttons for Zoom.
- Nodes' and links' style.
- Shadow around nodes.
- Expand button style.
- Tooltip with action buttons that contain svg icons.
- Event handlers: click node, expand/collapse node, add/delete btn click (see console and alerts).
- Some nodes are expanded by default.
- Blue frame is removed from main diagram and nodes.
- Filter for nodes. If filter is not empty, find nodes that match the filter and all its' direct parents and expand these nodes. Other nodes are hidden.
- Roots position is not changed while expanding/collapsing nodes.
- Nodes are ordered ascending by 'text' property.

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/41_GoJSCustomMindMap)

![41_GoJSCustomMindMap](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/41_GoJSCustomMindMap.png)

## 42. GoJS Custom MindMap. Nodes in two directions.

[GoJS Custom MindMap 2.0.4](https://codepen.io/sufflavus/full/KYzGWP) is based on 

- [GoJS Mind Map](https://gojs.net/latest/samples/mindMap.html)
- [Font Awesome v5](https://fontawesome.com/)
- [CSS](https://www.w3.org/Style/CSS/)

### Cutomized parts:
- There are two parts that render in two directions: right and left.
- Zoom on mouse wheel.
- Separate buttons for Zoom.
- Nodes' and links' style.
- Shadow around nodes.
- Expand button style.
- Tooltip with action buttons that contain svg icons.
- Event handlers: click node, expand/collapse node, add/delete btn click (see console and alerts).
- Some nodes are expanded by default.
- Blue frame is removed from main diagram and nodes.
- Filter for nodes. If filter is not empty, find nodes that match the filter and all its' direct parents and expand these nodes. Other nodes are hidden.
- The whole diagram can be moved by mouse.
- Scroll is removed from the diagram.
- It is possible to move the diagram out of bounds.
- Node links are not selectable.
- Move to the center note that has just been expanded/collapsed.

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/42_GoJSCustomMindMap)

![42_GoJSCustomMindMap](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/42_GoJSCustomMindMap.png)

## 43. Highcharts bubble chart.

[Highcharts bubble chart](https://codepen.io/sufflavus/full/vwbVBp) is based on 

- [Highcharts JS v8.2.0 (2020-08-20)](https://www.highcharts.com/demo/bubble)
- [jQuery 3.4.1](https://jquery.com/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/43_HighchartsBubbleChart)

![43_HighchartsBubbleChart_1](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/43_HighchartsBubbleChart_1.png)

## 44. Custom table styling

[Custom table styling](https://codepen.io/sufflavus/full/GaaVyJ) is based on 

- [CSS](https://www.w3.org/Style/CSS/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/44_CustomTableStyling)

![44_CustomTableStyling_1](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/44_CustomTableStyling_1.png)

## 45. Color converter

[Color converter](https://codepen.io/sufflavus/full/QWyNmVZ) is based on 

- [jQuery 3.5.1](https://jquery.com/) 
- [Materialize 1.0.0](https://materializecss.com) 

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/45_ColorConverter)

![45_ColorConverter_1](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/45_ColorConverter_1.png)

## 46. Multiline ellipsis

[Multiline ellipsis](https://codepen.io/sufflavus/full/yLezwzQ/) is based on 

- [CSS](https://www.w3.org/Style/CSS/)
- [Less](http://lesscss.org/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/46_MultilineEllipsis)

![46_MultilineEllipsis](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/46_MultilineEllipsis.png)

## 47. CSS circles

[CSS circles](https://codepen.io/sufflavus/full/YzqjWEo/) is based on 

- [CSS](https://www.w3.org/Style/CSS/)
- [Less](http://lesscss.org/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/47_CssCircles)

![47_CssCircles](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/47_CssCircles.png)

## 48. CSS Arrows

[CSS Arrows](https://codepen.io/sufflavus/full/poyQebR/) is based on 

- [CSS](https://www.w3.org/Style/CSS/)
- [Less](http://lesscss.org/)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/48_CssArrows)

![48_CssArrows](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/48_CssArrows.png)

## 49. Map of Finland (Regions)

[Map of Finland (Regions)](https://codepen.io/sufflavus/full/YzWwjWz) is based on 

- [GeoJson of Finland from TeemuKoivisto's repo](https://github.com/TeemuKoivisto/map-of-finland/blob/master/src/maakuntarajat-2018.json)
- [d3.js v3](https://d3js.org)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/49_MapOfFinland)

![49_MapOfFinland_2](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/49_MapOfFinland_2.png)

## 50. Heat map of Finland

[Heat map of Finland](https://codepen.io/sufflavus/full/gOMwxgw) is based on 

- [GeoJson of Finland from TeemuKoivisto's repo](https://github.com/TeemuKoivisto/map-of-finland/blob/master/src/kuntarajat-2018.json)
- [Statistics of regional income tax rates in 2014 is from tomimick's repo](https://github.com/tomimick/mapcolorizer/blob/master/data-finland/data/2014-tulovero.json)
- [d3.js 6.2.0](https://d3js.org)
- [d3-geo 1.9.1](https://github.com/d3/d3-geo)
- [d3-selection 1.2.0](https://github.com/d3/d3-selection)
- [d3-array 1.2.2](https://github.com/d3/d3-array)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/50_HeatMapOfFinland)

![50_HeatMapOfFinland_1](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/50_HeatMapOfFinland_1.png)

## 51. JSON-stat in table

[JSON-stat in table](https://codepen.io/sufflavus/full/wvWgBdG) is based on 

- [JSON-stat Format](https://json-stat.org/format)
- [JSON-stat file with test data is from json-stat web site](http://json-stat.org/samples/oecd-canada-col.json)
- [jQuery 3.5.1](https://jquery.com/) 
- [jsonstat-toolkit 1.0.8](https://www.npmjs.com/package/jsonstat-toolkit)

[Source and more screenshots](https://bitbucket.org/Sufflava/codepenexamples/src/master/51_JsonStatInTable)

![51_JsonStatInTable](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/thumbnail/51_JsonStatInTable.png)
