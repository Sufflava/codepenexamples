## 49. Map of Finland (Regions)

[Map of Finland (Regions)](https://codepen.io/sufflavus/full/YzWwjWz) is based on 

- [GeoJson of Finland from TeemuKoivisto's repo](https://github.com/TeemuKoivisto/map-of-finland/blob/master/src/maakuntarajat-2018.json)
- [d3.js](https://d3js.org/d3.v3.min.js)

![49_MapOfFinland_1](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/49_MapOfFinland_1.png)
![49_MapOfFinland_2](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/49_MapOfFinland_2.png)