(function(){
  // GeoJson of Finland is from TeemuKoivisto's repo (https://github.com/TeemuKoivisto/map-of-finland/blob/master/src/maakuntarajat-2018.json)
  // var mapUrl = "//gitcdn.link/repo/TeemuKoivisto/map-of-finland/master/src/kuntarajat-2018.json";
  // var mapUrl = "//cdn.jsdelivr.net/gh/TeemuKoivisto/map-of-finland@master/src/maakuntarajat-2018.json";
  
  var mapUrl = "//bitbucket.org/Sufflava/codepenexamples/raw/c9426e4d5b1ddfd3477dbb044e0a87f4c2aa9ddf/49_MapOfFinland/data/maakuntarajat-2018.json"

  var container = d3.select("#container")
    .node()
    .getBoundingClientRect(); 
  
  var width = container.width;
  var height = container.height;
  
  // https://bost.ocks.org/mike/map/ - instruction for drawing a map using d3 and geojson
  var projection = d3.geo.mercator()
    .scale(2000) // make the map X times bigger (the right scale should be found by hands)
    .translate([width*(-0.5) - 100, height*3.5 + 50]); // position the map inside the container (the right position should be found by hands)

  var path = d3.geo.path()
    .projection(projection);

  var svg = d3.select("#container")
    .append("svg")
    .attr({
      width: width,
      height: height
    });
  
  var tooltip = d3.select("body")
    .append("div")
    .attr({ class: "tooltip" })
    .text("");
  
  d3.json(mapUrl, function(geoJson) {
    svg.selectAll(".region")
      .data(geoJson.features)
      .enter()
      .append("path")
      .attr({
        class: "region",
        d: path
      })
      .on("mouseover", function(data) {
        return tooltip
          .style("visibility", "visible")
          .text(data.properties.NAMEFIN);
      })
      .on("mousemove", function() {
        return tooltip
          .style({
            top: (event.pageY - 45) + "px",
            left: event.pageX + "px",
            transform: "translateX(-50%)"
          });
      })
      .on("mouseout", function() {
         return tooltip.style("visibility", "hidden");
      });
  });
})();