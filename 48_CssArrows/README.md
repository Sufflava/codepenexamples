## 48. CSS Arrows

[CSS Arrows](https://codepen.io/sufflavus/full/poyQebR/) is based on 

- [CSS](https://www.w3.org/Style/CSS/)
- [Less](http://lesscss.org/)

![48_CssArrows](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/48_CssArrows.png)