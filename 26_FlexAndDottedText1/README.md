## 26. Flex and dotted text 1

[Flex and dotted text 1](http://codepen.io/sufflavus/full/yVPodz) is based on 

- [CSS](https://www.w3.org/Style/CSS/)

### The task is:

1. Blocks 1, 2 and 4 has fixed width based on content width.
2. Block 3 has to fill all available space.
3. Text in block 3 should be in one line. If text too long it has to be truncated with three dots.
4. If width of the screen is too small so there is no anought space for the forth block, the forth block should be moved to the next line.

![26_FlexAndDottedText1_1](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/26_FlexAndDottedText1_1.png)
![26_FlexAndDottedText1_2](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/26_FlexAndDottedText1_2.png)
![26_FlexAndDottedText1_3](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/26_FlexAndDottedText1_3.png)
