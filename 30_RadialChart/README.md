## 30. Radial chart

[Radial chart](https://codepen.io/sufflavus/full/EvdzwO/) is based on 

- [Highcharts 5.0.14](https://www.highcharts.com/demo/column-basic) 
- [Highcharts-more 5.0.14](http://code.highcharts.com/highcharts-more.js) 
- [jQuery 3.2.1](https://jquery.com/)

![30_RadialChart](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/30_RadialChart.png)
