## 33. Material range input (SeekBar)

[Material range input](https://codepen.io/sufflavus/full/gGvpqx/) is based on 

- [jQuery 3.2.1](https://jquery.com/)
- [Material design slider spec](https://material.io/guidelines/components/sliders.html)
- [Material design color spec](https://material.io/guidelines/style/color.html)

![33_MaterialRangeInput](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/33_MaterialRangeInput.png)
