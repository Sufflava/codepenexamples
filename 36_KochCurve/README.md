## 36. Koch Curve

[Koch Curve](https://codepen.io/sufflavus/full/GQeeOQ/) is based on 

- [Mathematical Koch Curve](https://de.wikipedia.org/wiki/Koch-Kurve)
- [d3.js 4.13.0](https://d3js.org)

![36_KochCurve](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/36_KochCurve.png)
