## 44. Custom table styling

[Custom table styling](https://codepen.io/sufflavus/full/GaaVyJ) is based on 

- [CSS](https://www.w3.org/Style/CSS/)

![44_CustomTableStyling_1](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/44_CustomTableStyling_1.png)
![44_CustomTableStyling_2](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/44_CustomTableStyling_2.png)