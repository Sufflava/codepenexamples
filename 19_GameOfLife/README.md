## 19. Game of Life

[Game of Life](http://codepen.io/sufflavus/full/grVeEE/) is based on 

- [jQuery 2.2.2](https://jquery.com/) 
- [Materialize 0.97.6](https://materializecss.com) 
- [React 15.0.2](https://reactjs.org) 
- [ReactDOM 15.0.2](https://reactjs.org/docs/react-dom.html)
- [Sass](http://sass-lang.com/)
- [Babel](https://babeljs.io/)
- [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)

![19_GameOfLife](https://bitbucket.org/Sufflava/codepenexamples/raw/master/images/19_GameOfLife.png)
